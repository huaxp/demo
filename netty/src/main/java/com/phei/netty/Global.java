package com.phei.netty;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;


public class Global {
	
	private static ChannelGroup group = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
	
	public static void add(Channel channel) {
		synchronized (group) {
			group.add(channel);
		}
	}
	
	public static void remove(Channel channel) {
		synchronized (group) {
			group.remove(channel);
		}
	}
	
	public static void write(Object message) {
		group.write(message);
	}
	
	public static void writeAndFlush(Object message) {
		group.writeAndFlush(message);
	}
	
	
}
