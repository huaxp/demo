package com.phei.netty.chat3;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;

import com.phei.netty.wss.SecureSslContextFactory;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslHandler;

public class SecureChatServerInitializer extends ChatServerInitalizer{
	
	private final SslContext context;
	private final boolean client;
	
	public SecureChatServerInitializer(ChannelGroup group,SslContext context,boolean client) {
		super(group);
		this.context = context;
		this.client = client;
	}

	
	@Override
	protected void initChannel(Channel ch) throws Exception {
		super.initChannel(ch);
		//SSLEngine engine = context.createSSLEngine();
		SSLEngine engine =context.newEngine(ch.alloc());
		/*SSLEngine  engine = SecureSslContextFactory .getServerContext(
				    "CA",
				    System.getProperty("user.dir") + "/server.keystore",
				    null,
				    "123456")
		    		.createSSLEngine();*/
		engine.setUseClientMode(client);
		engine.setNeedClientAuth(false);
		ch.pipeline().addFirst(new SslHandler(engine));
		
	}
	
	
}
