package com.phei.netty.chat3;

import com.phei.netty.chat.WebSocketHandler;
import com.phei.netty.wss.WebSocketServerHandler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

public class ChatServerInitalizer extends ChannelInitializer<Channel> {

	private ChannelGroup group;
	
	
	public ChatServerInitalizer(ChannelGroup group) {
		this.group = group;
	}
	
	@Override
	protected void initChannel(Channel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
		/*pipeline.addLast(new HttpServerCodec());
		pipeline.addLast(new ChunkedWriteHandler());
		pipeline.addLast(new HttpObjectAggregator(64*1024));
		//pipeline.addLast(new HttpRequestHandler("/ws"));
		//pipeline.addLast(new WebSocketServerProtocolHandler("/wss"));
		pipeline.addLast(new TextWebSocketFrameHandler(group));
		//pipeline.addLast(new WebSocketHandler());
*/		
	    pipeline.addLast("http-codec",    new HttpServerCodec());
	    pipeline.addLast("aggregator",	    new HttpObjectAggregator(65536));
	    ch.pipeline().addLast("http-chunked",    new ChunkedWriteHandler());
	    pipeline.addLast(new TextWebSocketFrameHandler(group));
	}

	
}
