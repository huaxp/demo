package com.phei.netty.chat4;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
/**
 *http://www.cnblogs.com/miller-zou/p/6995628.html 
 *
 */

public class WebsocketChatServer {

	    
	    private int port;
	    
	    public WebsocketChatServer(int port){
	        this.port = port;
	    }
	    
	    public void run() throws Exception{
	        EventLoopGroup bossGroup = new NioEventLoopGroup();
	        EventLoopGroup workerGroup = new NioEventLoopGroup();
	        try {
	            ServerBootstrap b = new ServerBootstrap();
	            b.group(bossGroup, workerGroup)
	             .channel(NioServerSocketChannel.class)
	             .childHandler(new WebsocketChatServerInitializer())
	             .option(ChannelOption.SO_BACKLOG, 128)
	             .childOption(ChannelOption.SO_KEEPALIVE, true);
	            
	            System.out.println("websocket start..");

	            ChannelFuture f = b.bind(port).sync();
	            
	            f.channel().closeFuture().sync();
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            workerGroup.shutdownGracefully();
	            bossGroup.shutdownGracefully();
	            System.out.println("websocket close.");
	        }
	    }
	    
	    public static void main(String[] args) throws Exception{
	        new WebsocketChatServer(8989).run();
	    }
}
