/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.phei.netty.wss;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;


import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Simple SSL chat client modified from {@link TelnetClient}.
 */
public class SecureClient 
{

    private final String host;
    private final int port;
    private final String sslMode;

    public SecureClient(String host, int port, String sslMode) {
		this.host = host;
		this.port = port;
		this.sslMode = sslMode;
    }

    public void run() throws Exception 
    {
		EventLoopGroup group = new NioEventLoopGroup();
		try 
		{
		    Bootstrap b = new Bootstrap();
		    b.group(group).channel(NioSocketChannel.class).handler(new SecureClientInitializer(sslMode));
		    // Start the connection attempt.
		    Channel ch = b.connect(host, port).sync().channel();
		    // Read commands from the stdin.
		    ChannelFuture lastWriteFuture = null;
		    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		    for (;;) 
		    {
		    	String line = in.readLine();
		    	if (line == null) {
		    		break;
		    	}
	
				// Sends the received line to the server.
				lastWriteFuture = ch.writeAndFlush(line + "\r\n");
		
				// If user typed the 'bye' command, wait until the server closes
				// the connection.
				if ("bye".equals(line.toLowerCase())) {
				    ch.closeFuture().sync();
				    break;
				}
		    }
	
		    // Wait until all messages are flushed before closing the channel.
		    if (lastWriteFuture != null) {
		    	lastWriteFuture.sync();
		    }
		} finally {
		    // The connection is closed automatically on shutdown.
		    group.shutdownGracefully();
		}
    }

    public static void main(String[] args) throws Exception
    {
		String addr = "127.0.0.1";
		if (args.length > 0) {
		    try {
		    	addr = args[0];
		    } catch (NumberFormatException e) {
		    	e.printStackTrace();
		    }
		}
    	
		int port = 8888;
		if (args.length > 1) {
		    try {
		    	port = Integer.parseInt(args[1]);
		    } catch (NumberFormatException e) {
		    	e.printStackTrace();
		    }
		}
		//new WebSocketServer().run(port);
		
		String sslMode = "CA";
		if (args.length > 2) {
		    try {
		    	sslMode = args[2];
		    } catch (NumberFormatException e) {
		    	e.printStackTrace();
		    }
		}

		new SecureClient(addr, port, sslMode).run();
    }
}
