
package com.phei.netty.wss;

import static io.netty.handler.codec.http.HttpHeaders.isKeepAlive;
import static io.netty.handler.codec.http.HttpHeaders.setContentLength;
import static io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PingWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;


public class WebSocketServerHandler extends SimpleChannelInboundHandler<Object>
{
    private static final Logger logger = Logger .getLogger(WebSocketServerHandler.class.getName());

    private WebSocketServerHandshaker handshaker;

    
   

    static final ChannelGroup channels = new DefaultChannelGroup( GlobalEventExecutor.INSTANCE );

    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws Exception 
    {
    	System.out.println("channelActive : ");
    	// Once session is secured, send a greeting and register the channel to the global channel
    	// list so the channel received the messages from others.
    	/*ctx.pipeline().get(SslHandler.class).handshakeFuture()
			.addListener(new GenericFutureListener<Future<Channel>>() {
		    @Override
		    public void operationComplete(Future<Channel> future)    throws Exception 
		    {
		    	ctx.writeAndFlush("Welcome to "	+ InetAddress.getLocalHost().getHostName()	+ " secure chat service!\n");
		    	ctx.writeAndFlush("Your session is protected by " + ctx.pipeline().get(SslHandler.class).engine() .getSession().getCipherSuite() + " cipher suite.\n");

		    	channels.add(ctx.channel());
		    }
		});*/
    	channels.add(ctx.channel());
    }

    
 

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
    	ctx.flush();
    }

    private void handleHttpRequest(ChannelHandlerContext ctx,   FullHttpRequest req) throws Exception
    {
    	System.out.println("handleHttpRequest : ");
		// 如果HTTP解码失败，返回HHTP异常
		if (!req.getDecoderResult().isSuccess()	|| (!"websocket".equals(req.headers().get("Upgrade")))) 
		{
		    sendHttpResponse(ctx, req, new DefaultFullHttpResponse(HTTP_1_1,  BAD_REQUEST));
		    return;
		}
	
		// 构造握手响应返回，本机测试
		WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory("wss://localhost:9999/websocket", null, false);
		handshaker = wsFactory.newHandshaker(req);
		if (handshaker == null) {
		    WebSocketServerHandshakerFactory .sendUnsupportedWebSocketVersionResponse(ctx.channel());
		} else {
		    handshaker.handshake(ctx.channel(), req);
		}
    }

    private void handleWebSocketFrame(ChannelHandlerContext ctx,   WebSocketFrame frame)
    {
    	
		// 判断是否是关闭链路的指令
		if (frame instanceof CloseWebSocketFrame) {
		    handshaker.close(ctx.channel(),  (CloseWebSocketFrame) frame.retain());
		    return;
		}
		// 判断是否是Ping消息
		if (frame instanceof PingWebSocketFrame) {
		    ctx.channel().write(new PongWebSocketFrame(frame.content().retain()));
		    return;
		}
		// 本例程仅支持文本消息，不支持二进制消息
		if (!(frame instanceof TextWebSocketFrame))
		{
		    throw new UnsupportedOperationException(String.format("%s frame types not supported", frame.getClass().getName()));
		}
	
		// 返回应答消息
		String request = ((TextWebSocketFrame) frame).text();
		if (logger.isLoggable(Level.FINE)) 
		{
		    logger.fine(String.format("%s received %s", ctx.channel(), request));
		}
		ctx.channel().write(new TextWebSocketFrame(request	+ " , WebSocket service Now at:" + new java.util.Date().toString()));
		System.out.println("handleWebSocketFrame : "+request);
    }

    private static void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest req, FullHttpResponse res) 
    {
		// 返回应答给客户端
		if (res.getStatus().code() != 200)
		{
		    ByteBuf buf = Unpooled.copiedBuffer(res.getStatus().toString(),  CharsetUtil.UTF_8);
		    res.content().writeBytes(buf);
		    buf.release();
		    setContentLength(res, res.content().readableBytes());
		}
	
		// 如果是非Keep-Alive，关闭连接
		ChannelFuture f = ctx.channel().writeAndFlush(res);
		if (!isKeepAlive(req) || res.getStatus().code() != 200) 
		{
		    f.addListener(ChannelFutureListener.CLOSE);
		}
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)  throws Exception {
    	System.out.println("exceptionCaught : ");
    	cause.printStackTrace();
    	ctx.close();
    }


	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
		System.out.println("WebSocket messageReceived : "+msg);
		// 传统的HTTP接入
		if (msg instanceof FullHttpRequest) {
		    handleHttpRequest(ctx, (FullHttpRequest) msg);
		}
		// WebSocket接入
		else if (msg instanceof WebSocketFrame) {
		    handleWebSocketFrame(ctx, (WebSocketFrame) msg);
		}
	}
}
