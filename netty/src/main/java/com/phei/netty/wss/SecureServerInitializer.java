/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.phei.netty.wss;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.ssl.util.SelfSignedCertificate;

import javax.net.ssl.SSLEngine;


import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;



import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;


/**
 * Creates a newly configured {@link ChannelPipeline} for a new channel.
 */
public class SecureServerInitializer extends 	ChannelInitializer<SocketChannel> 
{

    private String tlsMode;
    private String sProtocol;

    public SecureServerInitializer(String TLS_MODE, String spro)
    {
    	this.tlsMode = TLS_MODE;
    	this.sProtocol = spro;
    }

    @Override
    public void initChannel(SocketChannel ch) throws Exception 
    {
    	
    	
		ChannelPipeline pipeline = ch.pipeline();
	
		// Add SSL handler first to encrypt and decrypt everything.
		// In this example, we use a bogus certificate in the server side
		// and accept any invalid certificates in the client side.
		// You will need something more complicated to identify both
		// and server in the real world.
		//
		// Read SecureSslContextFactory
		// if you need client certificate authentication.
	
		SSLEngine engine = null;
		if (SSLMODE.CA.toString().equals(tlsMode)) 
		{
		    engine = SecureSslContextFactory .getServerContext(
				    tlsMode,
				    System.getProperty("user.dir") + "/server.keystore",
				    null,
				    "123456")
		    		.createSSLEngine();
		}
		else if (SSLMODE.CSA.toString().equals(tlsMode))
		{
		    engine = SecureSslContextFactory .getServerContext(
				    tlsMode,
				    System.getProperty("user.dir") + "/server.keystore",
				    System.getProperty("user.dir") + "/serverTrust.jks","123456")
		    		
		    		.createSSLEngine();
	
		    // engine = SecureSslContextFactory
		    // .getServerContext(
		    // tlsMode,
		    // System.getProperty("user.dir") + "/src/com/phei/netty/ssl/conf/client/sChat.jks",
		    // System.getProperty("user.dir") + "/src/com/phei/netty/ssl/conf/client/sChat.jks")
		    // .createSSLEngine();
		} else {
		    System.err.println("ERROR : " + tlsMode);
		    System.exit(-1);
		}
		
		/*SelfSignedCertificate cert = new SelfSignedCertificate();
		SslContext context =  SslContextBuilder.forServer(cert.certificate(), cert.privateKey()).build();
		engine =context.newEngine(ch.alloc());*/
		engine.setUseClientMode(false);
	
		// Client auth
		if (SSLMODE.CSA.toString().equals(tlsMode))
		    engine.setNeedClientAuth(true);
		
		pipeline.addLast("ssl", new SslHandler(engine));
	
	    
	    if (this.sProtocol.equalsIgnoreCase("TCP"))
	    {
	    	System.out.println("Use TCP");
			// On top of the SSL handler, add the text line codec.
			pipeline.addLast("framer", new DelimiterBasedFrameDecoder(8192,	Delimiters.lineDelimiter()));
			pipeline.addLast("decoder", new StringDecoder());
			pipeline.addLast("encoder", new StringEncoder());
		
			// and then business logic.
			pipeline.addLast("handler", new SecureChatServerHandler());
	    }
	    else
	    {
	    	System.out.println("Use WebSocket");
		    pipeline.addLast("http-codec",    new HttpServerCodec());
		    pipeline.addLast("aggregator",	    new HttpObjectAggregator(65536));
		    ch.pipeline().addLast("http-chunked",    new ChunkedWriteHandler());
		    pipeline.addLast("handler",	    new WebSocketServerHandler());
	    }

	    System.out.println("initChannel success-------------------");
	    
    }
}
