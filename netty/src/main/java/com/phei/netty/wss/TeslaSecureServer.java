/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.phei.netty.wss;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
//import io.netty.example.telnet.TelnetServer;



public class TeslaSecureServer {

    private final int port;
    private final String sProtocol;//websocket and  tcp
    private final String sslMode;

    public TeslaSecureServer(int port, String sslMode, String sProtocol) {
    	this.port = port;
    	this.sslMode = sslMode;
    	this.sProtocol = sProtocol;
    }

    public void run() throws InterruptedException 
    {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
		    ServerBootstrap b = new ServerBootstrap();
		    b.group(bossGroup, workerGroup)
			    .channel(NioServerSocketChannel.class)
			    .childHandler(new SecureServerInitializer(this.sslMode, this.sProtocol));
	
		    b.bind(port).sync().channel().closeFuture().sync();
		} finally {
		    bossGroup.shutdownGracefully();
		    workerGroup.shutdownGracefully();
		}
    }

    public static void main(String[] args) throws Exception {
		int port = 8888;
		if (args.length > 0) {
		    try {
		    	port = Integer.parseInt(args[0]);
		    } catch (NumberFormatException e) {
		    	e.printStackTrace();
		    }
		}
		//new WebSocketServer().run(port);
		
		String sslMode = "CA";
		if (args.length > 1) {
		    try {
		    	sslMode = args[1];
		    } catch (NumberFormatException e) {
		    	e.printStackTrace();
		    }
		}
		
		String sProtocol = "TCP";
		if (args.length > 2) {
		    try {
		    	sProtocol = args[2];
		    } catch (NumberFormatException e) {
		    	e.printStackTrace();
		    }
		}
		
		new TeslaSecureServer(port, sslMode, sProtocol).run();
    }
}
