/*package com.phei.netty.multicast2;

import java.net.InetSocketAddress;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;

public class LogEventClient {
	private EventLoopGroup group;
	private Bootstrap bootstrap;
	public LogEventClient(InetSocketAddress address) {
		group = new NioEventLoopGroup();
		bootstrap = new Bootstrap();
		bootstrap.group(group)
		.channel(NioDatagramChannel.class)
		.option(ChannelOption.SO_BROADCAST,true)
		.handler(new ChannelInitializer<Channel>() {

			@Override
			protected void initChannel(Channel channel) throws Exception {
				 ChannelPipeline pipeline = channel.pipeline();
				 pipeline.addLast(new LogEventDecoder());
				 pipeline.addLast(new LogEventHandleInitializer());
				 
			}
			
		}).localAddress(address);
	}
	public Channel bind() {
		return bootstrap.bind().syncUninterruptibly().channel();
	}
	
	public void stop() {
		group.shutdownGracefully();
	}
	
	public static void main(String[] args) {
		if(args.length !=1) {
			throw new IllegalArgumentException();
		}
		
		LogEventClient monitor = new LogEventClient(new InetSocketAddress(Integer.parseInt(args[0])));
		Channel channel = monitor.bind();
		System.out.println("LogEventMonitor running");
		try {
			channel.closeFuture().sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally {
			monitor.stop();
		}
	}
	
}
*/