/*package com.phei.netty.multicast2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.NetUtil;

public class LogEventServer {

		private EventLoopGroup group;
		private Bootstrap bootstrap;
		private File file;
		private InetSocketAddress groupAddress;
		 NetworkInterface ni = NetUtil.LOOPBACK_IF;
		
		public LogEventServer(InetSocketAddress groupAddress,File file) {
			
	            Enumeration<InetAddress> addresses = ni.getInetAddresses();
	            InetAddress localAddress = null;
	            while (addresses.hasMoreElements()) {
	                InetAddress address2 = addresses.nextElement();
	                if (address2 instanceof Inet4Address){
	                    localAddress = address2;
	                }
	            }
	        groupAddress = groupAddress;
			group = new NioEventLoopGroup();
			bootstrap = new Bootstrap();
			bootstrap.group(group)
			.channel(NioDatagramChannel.class)
			.localAddress(localAddress, groupAddress.getPort())
            .option(ChannelOption.IP_MULTICAST_IF, ni)
            .option(ChannelOption.SO_REUSEADDR, true)
			.handler(new LogEventEncoder(groupAddress));
			this.file = file;
		}
		public void run() {
			try {
				NioDatagramChannel ch = (NioDatagramChannel) bootstrap.bind(groupAddress.getPort()).sync().channel();
				ch.joinGroup(groupAddress, ni).sync();
		        System.out.println("server");
		         ch.closeFuture().await();
				long pointer = 0;
				for(;;) {
					long len = file.length();
					System.out.println("len:"+len);
					if(len < pointer) {
						pointer = len;
					}else if(len > pointer) {
						RandomAccessFile raf = new RandomAccessFile(file, "r");
						raf.seek(pointer);
						String line;
						while((line = raf.readLine()) != null) {
						ch.writeAndFlush(new LogEvent(null, -1,file.getAbsolutePath(),"dddddddddddddddd"));
						}
						pointer = raf.getFilePointer();
						raf.close();
					}
					
					Thread.sleep(5000);
					
				}
				
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.interrupted();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				Thread.interrupted();
			} catch (IOException e) {
				e.printStackTrace();
				Thread.interrupted();
			}
			
		}
		public void stop() {
			group.shutdownGracefully();
		}
		
		public static void main(String[] args) {
			if(args.length != 2) {
				throw new IllegalArgumentException();
			}
			LogEventServer broadCaster = new LogEventServer(new InetSocketAddress("127.0.0.1",Integer.parseInt(args[0])), new File(args[1]));
			
			try {
				broadCaster.run();
			}
			finally {
				broadCaster.stop();
			}
		}
		
		
}
*/