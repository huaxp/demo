package com.phei.netty.broadcast;

import java.net.InetSocketAddress;
import java.util.HashMap;

public class LogEvent {
    public static final byte SEPARATOR = (byte) ':';
    private  InetSocketAddress address = null;
    private  String logfile= null;
    private  String msg = null;
    private  long received = 0l;
    public LogEvent(InetSocketAddress address,long received, String logfile,String msg){
        this.address = address;
        this.received = received;
        this.logfile = logfile;
        this.msg = msg;
    }

    public LogEvent(String logfile,String msg){
        this(null,-1,logfile,msg);
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public String getLogfile() {
        return logfile;
    }

    public String getMsg() {
        return msg;
    }

    public long getReceived() {
        return received;
    }


}
