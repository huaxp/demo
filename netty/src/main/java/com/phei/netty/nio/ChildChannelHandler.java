package com.phei.netty.nio;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class ChildChannelHandler extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel arg0) throws Exception {
        System.out.println("server initChannel..");
        arg0.pipeline().addLast(new TimeServerHandler());
    }
}