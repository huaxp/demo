package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;


@SpringBootApplication
@ImportResource(value={"classpath:spring_dubbo_provider.xml"})
public class ShenmaProviderApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ShenmaProviderApplication.class);
	}

	public static void main(String[] args) throws Exception {
		
		SpringApplication.run(ShenmaProviderApplication.class, args);
	}

}