package com.test;

import org.junit.Ignore;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class HessianTest {
    
	 ClassPathXmlApplicationContext context;
	    public HessianTest(){
	        context=new ClassPathXmlApplicationContext(new String[]{
	                "classpath*:spring_dubbo_consumer.xml"
	        });
	    }
	    
	    @org.junit.Test
	    public void Test(){
	        HessianTestService hessian = (HessianTestService) context.getBean("hessian");
	        String sayHello = hessian.sayHello("ni hao test is pass");
	        System.out.println("\nresult is "+sayHello);
	    }
	    

}