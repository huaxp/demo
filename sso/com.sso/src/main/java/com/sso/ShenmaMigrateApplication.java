package com.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;


//@ImportResource(value={"classpath:/ehcache-shiro.xml"})
@SpringBootApplication
public class ShenmaMigrateApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ShenmaMigrateApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ShenmaMigrateApplication.class, args);
	}

}